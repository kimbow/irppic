import { FotoService } from './servicos/foto.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FotoModule } from './foto/foto.module';
import { PainelModule } from './painel/painel.module';
import { CadastroComponent } from './cadastro/cadastro.component';
import { ListagemComponent } from './listagem/listagem.component';
import { roteamento } from './app.routes';

@NgModule({
  declarations: [AppComponent, CadastroComponent, ListagemComponent], // Declara componentes
  imports: [ // Importa módulos
    BrowserModule
    ,FotoModule
    ,HttpClientModule
    ,PainelModule
    ,roteamento
    ,FormsModule
  ],
  providers: [FotoService], // Provisiona objetos injetáveis
  bootstrap: [AppComponent]
})
export class AppModule { }
