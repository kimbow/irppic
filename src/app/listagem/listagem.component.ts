import { FotoComponent } from "./../foto/foto.component";
import { Component } from "@angular/core";
import { FotoService } from "./../servicos/foto.service";

@Component({
  selector: "listagem",
  templateUrl: "./listagem.component.html"
})
export class ListagemComponent {
  title = "Galeria de Iagens";
  listaFotos;
  mensagem: string;

  constructor(private servico: FotoService) {
    servico
      .listar()
      .subscribe(
        fotosApi => (this.listaFotos = fotosApi),
        erro => console.log(erro)
      );
  }

  remover(foto: FotoComponent): void {
    this.servico
      .deletar(foto)
      .subscribe(
        () => { 
                this.listaFotos.splice(this.listaFotos.indexOf(foto),1);
                this.mensagem = `Foto ${foto.titulo} apagada com sucesso!`;
                setTimeout(() => this.mensagem = '',5000);
                console.log(`Foto ${foto.titulo} apagada com sucesso!`);
            }
        , erro => {
            this.mensagem = `Erro ao apagar a Foto ${foto.titulo}!`;
            setTimeout(() => this.mensagem = '',5000);
            console.log(erro);
        }
      );
  }
}
